import XCTest
@testable import abpfilterparser_swift

final class RequestFilterParserTests: XCTestCase {

    func test_sitekeyPresent_filterIsInvalid() {
        XCTAssertEqual(Filter.from("bla$match-case,csp=first csp,script,other,third-party,domain=FOO.cOm,sitekey=foO"),
                       .invalid("bla$match-case,csp=first csp,script,other,third-party,domain=FOO.cOm,sitekey=foO",
                                "Filter with sitekey is not supported"))
    }

    func test_filterOptions() {
        compareFilter("bla$match-case,csp=first csp,script,other,third-party,domain=FOO.cOm",
                      pattern: "bla",
                      options: [.script, .other],
                      domains: [.included("FOO.cOm")],
                      matchase: true,
                      thirdParty: true)

        compareFilter("bla$~match-case,~csp=csp,~script,~other,~third-party,domain=~bAr.coM",
                      pattern: "bla",
                      options: RequestContentType.resourceTypes.subtracting([.script, .other]),
                      domains: [.excluded("bAr.coM")],
                      matchase: false,
                      thirdParty: false)

        compareFilter("@@bla$match-case,script,other,third-party,domain=foo.com|bar.com|~bAR.foO.Com|~Foo.Bar.com",
                      pattern: "bla",
                      options: [.script, .other],
                      domains: [.included("foo.com"), .included("bar.com"), .excluded("bAR.foO.Com"), .excluded("Foo.Bar.com")],
                      matchase: true,
                      thirdParty: true,
                      allowing: true)

        compareFilter("@@bla$~script,~other",
                      pattern: "bla",
                      options: RequestContentType.resourceTypes.subtracting([.script, .other]),
                      allowing: true)

        compareFilter("@@http://bla$~script,~other",
                      pattern: "http://bla",
                      options: RequestContentType.resourceTypes.subtracting([.script, .other]),
                      allowing: true)

        compareFilter("@@ftp://bla$~script,~other",
                      pattern: "ftp://bla",
                      options: RequestContentType.resourceTypes.subtracting([.script, .other]),
                      allowing: true)

        compareFilter("@@bla$~script,~other,document",
                      pattern: "bla",
                      options: RequestContentType.resourceTypes.subtracting([.script, .other]).union([.document]),
                      allowing: true)

        compareFilter("@@bla$~script,~other,~document",
                      pattern: "bla",
                      options: RequestContentType.resourceTypes.subtracting([.script, .other]),
                      allowing: true)

        compareFilter("@@bla$document",
                      pattern: "bla",
                      options: [.document],
                      allowing: true)

        compareFilter("@@bla$~script,~other,elemhide",
                      pattern: "bla",
                      options: RequestContentType.resourceTypes.subtracting([.script, .other]).union([.elemhide]),
                      allowing: true)

        compareFilter("@@bla$~script,~other,~elemhide",
                      pattern: "bla",
                      options: RequestContentType.resourceTypes.subtracting([.script, .other]),
                      allowing: true)

        compareFilter("@@bla$elemhide",
                      pattern: "bla",
                      options: [.elemhide],
                      allowing: true)


        XCTAssertEqual(Filter.from("@@bla$~script,~other,donottrack"),
                       .invalid("@@bla$~script,~other,donottrack", "Unknowkn filter option donottrack"))
        XCTAssertEqual(Filter.from("@@bla$~script,~other,~donottrack"),
                       .invalid("@@bla$~script,~other,~donottrack", "Unknowkn filter option donottrack"))
        XCTAssertEqual(Filter.from("@@bla$donottrack"),
                       .invalid("@@bla$donottrack", "Unknowkn filter option donottrack"))
        XCTAssertEqual(Filter.from("@@bla$foobar"),
                       .invalid("@@bla$foobar", "Unknowkn filter option foobar"))
        XCTAssertEqual(Filter.from("@@bla$image,foobar"),
                       .invalid("@@bla$image,foobar", "Unknowkn filter option foobar"))
        XCTAssertEqual(Filter.from("@@bla$foobar,image"),
                       .invalid("@@bla$foobar,image", "Unknowkn filter option foobar"))
    }

    func test_filterWithWildcard() {
        // Blocking
        compareFilter("||example.com^$domain=example.*",
                      pattern: "||example.com^",
                      domains: [.included("example.*")])
        compareFilter("||example.com^$domain=example.*|example.net",
                      pattern: "||example.com^",
                      domains: [.included("example.*"), .included("example.net")])
        compareFilter("||example.com^$domain=example.net|example.*",
                      pattern: "||example.com^",
                      domains: [.included("example.*"), .included("example.net")])
        compareFilter("||example.com^$domain=~example.net|example.*",
                      pattern: "||example.com^",
                      domains: [.included("example.*"), .excluded("example.net")])
        compareFilter("||example.com^$domain=example.*|~example.net",
                      pattern: "||example.com^",
                      domains: [.included("example.*"), .excluded("example.net")])

        // Allowing
        compareFilter("@@||example.com^$domain=example.*",
                      pattern: "||example.com^",
                      domains: [.included("example.*")],
                      allowing: true)
        compareFilter("@@||example.com^$domain=example.*|example.net",
                      pattern: "||example.com^",
                      domains: [.included("example.*"), .included("example.net")],
                      allowing: true)
        compareFilter("@@||example.com^$domain=example.net|example.*",
                      pattern: "||example.com^",
                      domains: [.included("example.*"), .included("example.net")],
                      allowing: true)
        compareFilter("@@||example.com^$domain=~example.net|example.*",
                      pattern: "||example.com^",
                      domains: [.included("example.*"), .excluded("example.net")],
                      allowing: true)
        compareFilter("@@||example.com^$domain=example.*|~example.net",
                      pattern: "||example.com^",
                      domains: [.included("example.*"), .excluded("example.net")],
                      allowing: true)
    }

    // MARK: - Utils
    func compareFilter(_ text: String,
                       pattern: String?,
                       options: Set<RequestContentType> = RequestContentType.resourceTypes,
                       domains: Set<Domain> = [],
                       matchase: Bool = false,
                       thirdParty: Bool = false,
                       allowing: Bool = false) {
        XCTAssertEqual(Filter.from(text),
                       .requestFilter(.init(pattern: pattern,
                                            options: options,
                                            domains: domains,
                                            matchase: matchase,
                                            thirdParty: thirdParty,
                                            allowing: allowing)))
    }
}
