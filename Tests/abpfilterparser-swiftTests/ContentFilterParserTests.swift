import XCTest
@testable import abpfilterparser_swift

final class ContentFilterParserTests: XCTestCase {
    func test_comment() {
        let comment = "! Example of commnet"
        XCTAssertEqual(Filter.from(comment), .comment(comment))
    }

    func test_parseBlockingContentFilter() {
        compareFilter("###ddd",
                      selector: "#ddd")
        compareFilter("##body > div:first-child",
                      selector: "body > div:first-child")
        compareFilter("fOO##ddd",
                      domains: [.included("fOO")],
                      selector: "ddd")
        compareFilter("Foo,bAr##ddd",
                      domains: [.included("Foo"), .included("bAr")],
                      selector: "ddd")
        compareFilter("foo,~baR##ddd",
                      domains: [.included("foo"), .excluded("baR")],
                      selector: "ddd")
        compareFilter("foo,~baz,bar##ddd",
                      domains: [.included("foo"), .excluded("baz"), .included("bar")],
                      selector: "ddd")
    }

    func test_parseAllowingContentFilter() {
        compareFilter("#@#ddd",
                      selector: "ddd",
                      isAllowing: true)
        compareFilter("#@#body > div:first-child",
                      selector: "body > div:first-child",
                      isAllowing: true)
        compareFilter("fOO#@#ddd",
                      domains: [.included("fOO")],
                      selector: "ddd",
                      isAllowing: true)
        compareFilter("Foo,bAr#@#ddd",
                      domains: [.included("Foo"), .included("bAr")],
                      selector: "ddd",
                      isAllowing: true)
        compareFilter("foo,~baR#@#ddd",
                      domains: [.included("foo"), .excluded("baR")],
                      selector: "ddd",
                      isAllowing: true)
        compareFilter("foo,~baz,bar#@#ddd",
                      domains: [.included("foo"), .excluded("baz"), .included("bar")],
                      selector: "ddd",
                      isAllowing: true)

    }

    func test_unsupportedContentFiltersAreInvalid() {
        XCTAssertEqual(Filter.from("#?#ddd"), .invalid("#?#ddd", "Element hiding emulation filter is not supported"))
        XCTAssertEqual(Filter.from("#$#ddd"), .invalid("#$#ddd", "Snippet filter is not supported"))
    }

    func test_filtersWithEmptyDomainNameAreInvalid() {
        let rawFilters = [
            ",##selector", ",,,##selector", "~,foo.com##selector", "foo.com,##selector",
            ",foo.com##selector", "foo.com,~##selector",
            "foo.com,,bar.com##selector", "foo.com,~,bar.com##selector"
        ]

        for rawFilter in rawFilters {
            XCTAssertEqual(Filter.from(rawFilter), .invalid(rawFilter, "Empty domain name is not allowed"))
        }
    }

    func test_contentFilterWithBraces() {
        compareFilter("###foo{color: red}", selector: "#foo{color: red}")
    }

    func test_blockingContentFiltersWithWildcards() {
        compareFilter("example.*##abc",
                      domains: [.included("example.*")],
                      selector: "abc")
        compareFilter("example.*,example.net##abc",
                      domains: [.included("example.*"), .included("example.net")],
                      selector: "abc")
        compareFilter("example.net,example.*##abc",
                      domains: [.included("example.*"), .included("example.net")],
                      selector: "abc")
        compareFilter("~example.net,example.*##abc",
                      domains: [.included("example.*"), .excluded("example.net")],
                      selector: "abc")
        compareFilter("example.*,~example.net##abc",
                      domains: [.included("example.*"), .excluded("example.net")],
                      selector: "abc")
    }

    func test_allowingContentFiltersWithWildcards() {
        compareFilter("example.*#@#abc",
                      domains: [.included("example.*")],
                      selector: "abc",
                      isAllowing: true)
        compareFilter("example.*,example.net#@#abc",
                      domains: [.included("example.*"), .included("example.net")],
                      selector: "abc",
                      isAllowing: true)
        compareFilter("example.net,example.*#@#abc",
                      domains: [.included("example.*"), .included("example.net")],
                      selector: "abc",
                      isAllowing: true)
        compareFilter("~example.net,example.*#@#abc",
                      domains: [.included("example.*"), .excluded("example.net")],
                      selector: "abc",
                      isAllowing: true)
        compareFilter("example.*,~example.net#@#abc",
                      domains: [.included("example.*"), .excluded("example.net")],
                      selector: "abc",
                      isAllowing: true)
    }

    // MARK: - Utils
    
    // Assert text can be parsed as Content Filter with expected domains, selector and allowed flag
    func compareFilter(_ text: String,
                       domains: Set<Domain> = [],
                       selector: String,
                       isAllowing: Bool = false) {
        XCTAssertEqual(Filter.from(text),
                       .contentFilter(.init(domains: domains, selector: selector, isAllowing: isAllowing)))
    }
}
