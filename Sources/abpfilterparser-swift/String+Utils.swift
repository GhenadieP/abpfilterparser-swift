import Foundation

extension String {
    var range: NSRange {
        NSRange(startIndex..., in: self)
    }

    var regex: NSRegularExpression? {
        try? NSRegularExpression(pattern: self, options: .caseInsensitive)
    }
}
