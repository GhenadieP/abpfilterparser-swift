extension Filter {

    /// Parse the given text to a filter if possible
    /// - Parameter text: Text to be parsed
    /// - Returns: The parsed filter, if text is in unknown format an invalid filter will be return
    public static func from(_ text: String) -> Filter {
        if text.first == "!" {
            return comment(text)
        }

        let match = text.contains("#") ? text.matchContentRegExp() : nil
        if let match = match {
            return parseContentFilter(text, match.domains, match.type, match.selector)
        } else {
            return parseRequestFilter(from: text)
        }
    }
}

extension Filter {
    static func parseContentFilter(_ text: String,
                                   _ domains: Substring?,
                                   _ type: Substring?,
                                   _ selector: Substring) -> Filter {
        // Validate domains
        if let domains = domains, // Domains are matched
           !domains.isEmpty, // Domains substring is not empty
           String(domains).test(against: domainsValidationRegExp) {
            return invalid(text, "Empty domain name is not allowed")
        }

        // Parse filter
        switch type {
        case .none:
            let domains = domains?.extractDomains(separator: ",") ?? []
            return contentFilter(.init(domains: domains, selector: String(selector), isAllowing: false))
        case "@":
            let domains = domains?.extractDomains(separator: ",") ?? []
            return contentFilter(.init(domains: domains, selector: String(selector), isAllowing: true))
        case "?":
            return invalid(text, "Element hiding emulation filter is not supported")
        case "$":
            return invalid(text, "Snippet filter is not supported")
        case let .some(unknown):
            // Should not happen based on `contentRegExp which specifies the type to be one of [@?$]
            return invalid(text, "Unkown content filter type \(unknown)")
        }
    }

    static func parseRequestFilter(from text: String) -> Filter {
        var rawFilter = text
        var blocking = true

        if rawFilter.hasPrefix("@@") {
            blocking = false
            // Consume allowing prefix
            rawFilter.removeFirst(2)
        }

        var rawPattern = rawFilter[...]
        var contentTypes = Set<RequestContentType>()
        var domains = Set<Domain>()
        var matchCase = false
        var thirdParty = false

        let optionsMatch = rawFilter.contains("$") ? rawFilter.matchOptionsRegExp() : nil

        if let optionsMatch = optionsMatch {
            let options = optionsMatch.split(separator: ",")
            // selector is the text upto `$`.
            rawPattern = rawFilter.prefix(upTo: rawFilter.index(before: optionsMatch.startIndex))

            for var option in options {
                var value: Substring?
                if let separatorIndex = option.firstIndex(of: "=") {
                    value = option[option.index(after: separatorIndex)...]
                    option = option.prefix(upTo: separatorIndex)
                }

                let inverse = option.first == "~"
                if inverse {
                    option.removeFirst(1)
                }

                if let type = RequestContentType(rawValue: String(option)) {
                    if inverse {
                        if contentTypes.isEmpty {
                            contentTypes = RequestContentType.resourceTypes
                        }
                        contentTypes.remove(type)
                    } else {
                        contentTypes.insert(type)
                        // Additional processing for CSP and header is not done, since those are not supported
                    }
                } else {
                    switch option {
                    case "match-case":
                        matchCase = !inverse
                    case "domain":
                        guard let value = value else { return invalid(text, "Unspecified domains list")}
                        // Note: For some reason domains are not checked if empty for request filters
                        domains = value.extractDomains(separator: "|")
                    case "third-party":
                        thirdParty = !inverse
                    case "csp":
                        // Not supported, just ignore.
                        break
                    case "sitekey":
                        // Not supported, breaks the whole filter
                        return invalid(text, "Filter with sitekey is not supported")
                    case "rewrite":
                        // Not supported, just ignore.
                        break
                    default:
                        // Unkown options, breaks the whole filter
                        return invalid(text, "Unknowkn filter option \(option)")
                    }
                }
            }
        }

        // If no content type is provided, then all resource types do apply
        if contentTypes.isEmpty {
            contentTypes = RequestContentType.resourceTypes
        }

        // No additional processing is done for csp and rewrite, as those are not supported

        return requestFilter(.init(pattern: String(rawPattern),
                                   options: contentTypes,
                                   domains: domains,
                                   matchase: matchCase,
                                   thirdParty: thirdParty,
                                   allowing: !blocking))
    }
}
