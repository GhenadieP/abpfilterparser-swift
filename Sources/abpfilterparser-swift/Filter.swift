
// Filter types which can be parsed from a string
public enum Filter: Equatable {
    // Commment filter of form `! Some comment`
    case comment(_ text: String)

    // Any filter which can be put in other categories
    case invalid(_ text: String, _ reason: String)

    /// Content filter of form `domain1.com,~domain2.com##selector` or `domain1.com,~domain2.com#@#selector`
    case contentFilter(ContentFilter)

    // Request filter of form selector$options or @@selector$options
    case requestFilter(RequestFilter)
}

public struct ContentFilter: Equatable {
    let domains: Set<Domain>
    let selector: String
    let isAllowing: Bool
}

public struct RequestFilter: Equatable {
    let pattern: String?
    let options: Set<RequestContentType>
    let domains: Set<Domain>
    let matchase: Bool
    let thirdParty: Bool
    let allowing: Bool
}

public enum RequestContentType: String, Equatable {
    case other = "other"
    case script = "script"
    case image = "image"
    case stylesheet = "stylesheet"
    case object = "object"
    case subdocument = "subdocument"
    case websocket = "websocket"
    case webrtc = "webrtc"
    case ping = "ping"
    case xmlhttprequest = "xmlhttprequest"
    case media = "media"
    case font = "font"
    case popup = "popup"
    //    case csp = "csp" // not supported
    //    case header = "header" // not supported
    case document = "document"
    case genericblock = "genericblock"
    case elemhide = "elemhide"
    case generichide = "generichide"

    static var resourceTypes: Set<RequestContentType> {
        [.other, .script, .image,
         .stylesheet, .object, .subdocument,
         .websocket, .webrtc, .ping, .media, .font]
    }
}
