import Foundation

public enum Domain: Hashable {
    case included(String)
    case excluded(String)
}

extension Substring {
    func extractDomains(separator: Character) -> Set<Domain> {
        let domains = split(separator: separator).map { domain -> Domain in
            if domain.first == "~" {
                return Domain.excluded(String(domain.dropFirst()))
            } else {
                return Domain.included(String(domain))
            }
        }
        return Set(domains)
    }
}
