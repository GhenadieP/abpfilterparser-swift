import Foundation

extension String {
    func matchContentRegExp() -> (domains: Substring?, type: Substring?, selector: Substring)? {

        guard let match = contentRegExp.firstMatch(in: self, options: [], range: range) else {
            // No match
            return nil
        }

        guard let selectorRange = Range(match.range(at: 3), in: self) else {
            // Invalid match, selector should exist
            return nil
        }

        // Domains can be missing
        let domains = Range(match.range(at: 1), in: self).map { self[$0] }
        // If type is missing, then it is Element Hiding filter
        let type = Range(match.range(at: 2), in: self).map { self[$0] }
        let selector = self[selectorRange]

        return (domains, type, selector)
    }

    func matchOptionsRegExp() -> Substring? {
        guard let match = optionsRegExp.firstMatch(in: self, options: [], range: range) else {
            // No match
            return nil
        }
        return Range(match.range(at: 1), in: self).map { self[$0] }
    }

    func test(against regex: NSRegularExpression) -> Bool {
        regex.firstMatch(in: self, options: [], range: range) != nil ? true : false
    }
}
