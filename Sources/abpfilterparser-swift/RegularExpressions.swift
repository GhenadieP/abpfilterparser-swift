import Foundation

// Globaly defined regular expressions to be shared betwen filter parsers.

/// Regular expression that content filters should match
let contentRegExp = "^([^/|@\"!]*?)#([@?$])?#(.+)$".regex!

/// Validates the domains list for content filter. No empty domain is allowed in the list.
let domainsValidationRegExp = "(^|,)~?(,|$)".regex!

/// Regular expression that request filter options should match
let optionsRegExp = "\\$(~?[\\w-]+(?:=[^,]*)?(?:,~?[\\w-]+(?:=[^,]*)?)*)$".regex!
