# abpfilterparser-swift

A simple showcase of parsing abp filters written in swift. Based on https://gitlab.com/eyeo/adblockplus/adblockpluscore/-/blob/next/lib/filterClasses.js.
Note: Filter header is not parsed, as it is out of scope for this POC.

## POC findings

### Filter parsing can be accomodated with Swift

Translating adblockpluscore js code to Swift was a fairly easy and straightforward process. All the relevant tests from adblockpluscore are passing with Swift implementation.

### A problem

Regex. If we are to rewrite the parsing in Swift we should move away from Regex based parsing: 
- Swift does not have an up to date Regex mechanism, instead NSRegularExpression is used.
- Regex parsing doesn't look to be a good option when performance is concerned.
- For optimal usage, a regex should be compiled with NSRegularExpression and reused across multiple parsing operations.
- Some Regex expressions are convoluted and not straightforward.
- NSRegularExpression does not work on substrings, thus requires creating intermediary Strings.

### A possible solution

A solution would be to make use of Substring to efficiently extract components from a filter. Even more we could parse the UTF8View of a filter, and use ArraySlices to extract components from an UTF8View. Those options will require more work, but we'll have a better control over the parsing correctness and performance.

### A hidden benfit

By specializing filter parsing to BlockingRules, we can remove any intermediary when parsing a Filter to a BlockingRule. Right now, in abp2blocklist, a Filter is parsed to a specific FilterClass, then a FilterClass is converted to a BlockingRule; instead we could parse directly to Blocking Rule components:

- `matchase` filter option can be directly parsed to `triger.url-filter-is-case-sensitive`.
- `third-party` filter option can be directly parsed to `trigger.load-type`.
- Resource type options(`image`, `script`...) and their negated version(`~image`, `~script`...) can be directly parsed to their `trigger.resource-type` counterpart.
- Filter blocking config(`##` or `someUrl/..`) can be directly parsed to a given `action.type.css-display-none` or `action.type.block`.
- Filter allowing config(`#@#` or `@@`) can be directly parsed to a given `action.type.ignore-previous-rules`.
- CSS selector can be directly parsed to `action.selector`
- `~somedomain` can be directly parsed to `unless-domain`
- `somedomain` can be directly parsed to `if-domain`
- `pattern` or `regex` could be directly parsed to `url-filter`. This might need additional accomodation, like proper conversion to regExp.

This could improve the performance, and remove some indirection on conversion process when an intermediary data structure is involved.

### Performance

Note: https://github.com/google/swift-benchmark was used

easylist.txt, parsing(not with conversion to BlockingRules) performance is acceptable, averaging around 0.19 seconds; easylist.txt has 56547 total filters.
It can be further reduced by slicing the filter list in more chunks and do the parsing in parallel. By slicing in 5 chunks, the parsing time can be as fast as 0.06 seconds.
The performance possibly could be even better if we are to remove the usage of Regex.

