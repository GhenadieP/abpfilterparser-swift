import XCTest

import performanceTests

var tests = [XCTestCaseEntry]()
tests += performanceTests.allTests()
XCTMain(tests)
