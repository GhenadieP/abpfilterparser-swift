// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "performance",
    dependencies: [
        .package(
          name: "Benchmark",
          url: "https://github.com/google/swift-benchmark",
          .branch("main")
        ),
        .package(name: "abpfilterparser-swift", path: "../")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "performance",
            dependencies: [.product(name: "Benchmark", package: "Benchmark"),
                           .product(name: "abpfilterparser-swift", package: "abpfilterparser-swift")],
            resources: [.copy("Resources/easylist-minified.txt"), .copy("Resources/filters.txt")]),
        .testTarget(
            name: "performanceTests",
            dependencies: ["performance"]),
    ]
)
