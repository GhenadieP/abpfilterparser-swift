import Benchmark
import abpfilterparser_swift
import Foundation

// Setup
let fileURL = Bundle.module.url(forResource: "filters", withExtension: "txt")
let rawFilters = try! String(contentsOf: fileURL!).split(separator: "\n").map(String.init)
let filterChunks = rawFilters.chunked(into: 5)
let dispatchGroup = DispatchGroup()

// Benchmarks

benchmark("simple parsing") {
    _ = rawFilters.map(Filter.from)
}

benchmark("Parallel parsing") {
    for chunk in filterChunks {
        dispatchGroup.enter()
        DispatchQueue.global().async {
            _ = chunk.map(Filter.from)
            dispatchGroup.leave()
        }
    }
    dispatchGroup.wait()
}

Benchmark.main()
